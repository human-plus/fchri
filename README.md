# fcHRI

Forza fcHRI: Functional connectivity during human-robot interaction

Repository for: 

"Domain-specific and domain-general neural network engagement during human-robot interactions" by
[Ann Hogenhuis](https://twitter.com/AnnHogenhuis) and [Ruud Hortensius](https://twitter.com/RuudHortensius).

Data: https://osf.io/mea7n/   
Preprint: https://psyarxiv.com/rus2w/

