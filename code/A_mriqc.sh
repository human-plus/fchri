#!/bin/bash

#User inputs:
#subj=01
#nthreads=2

#Make mriqc directory and participant directory in derivatives folder
$filenames = Get-ChildItem C:\Users\\LabUser\Desktop\DataFolder\fcHRI\derivatives | select -expand fullname
$filenames -match "mriqc"
If ($filenames -eq 'False') {
md 'C:\Users\\LabUser\Desktop\DataFolder\fcHRI\derivatives\mriqc' 
}

$subjectnames = Get-ChildItem C:\Users\\LabUser\Desktop\DataFolder\fcHRI\derivatives\mriqc | select -expand fullname
$subjectnames -match "sub-01"
If ($subjectnames -eq 'False') {
md 'C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives\mriqc\sub-01' 
}

#Run MRIQC
echo ""
echo "Running MRIQC on participant $s"
echo ""

docker run -it --rm -v C:\Users\LabUser\Desktop\DataFolder\fcHRI:/data:ro -v C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives\mriqc\sub-01:/out poldracklab/mriqc:latest /data /out participant --participant_label 01 --float32 --fft-spikes-detector C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives\mriqc\sub-01

