#!/bin/bash
#Template provided by Daniel Levitas of Indiana University
#Edits by Ann Hogenhuis, Utrecht University, 10.04.2020

#User inputs:
fmriprep-docker C:\Users\LabUser\Desktop\DataFolder\fcHRI C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives participant --participant-label 19 20 --fs-license-file C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives\licenses\license.txt  --skip_bids_validation --use-syn-sd --fs-no-reconall
