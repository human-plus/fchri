% Create a group average for the GM_probseg.nii
% Run this in MATLAB):

%========================================================================
%     Create group mask before 2nd level analysis in BIDS format
%========================================================================
%     This script is written by Ann Hogenhuis and Ruud Hortensius
%     This script is based on a script by Ruud Hortensius and Michaela Kent
%     (University of Glasgow)
%
%     Last updated: November 2020
%========================================================================



%{, eval = FALSE} 
clear all
 
spm('Defaults','fMRI'); %Initialise SPM fmri
spm_jobman('initcfg');  %Initialise SPM batch mode
 
BIDS = spm_BIDS('C:\Users\LabUser\Desktop\DataFolder\fcHRI'); %change this 
BIDSfirst=fullfile(BIDS.dir,'derivatives\fmriprep')
 
sublist = transpose(BIDS.participants.participant_id) 
subex = [] %subjects that don?t have an anatomical (14 dataset_1) 
sublist(subex) = [];
 
for i=1:length(sublist)
    subdir=fullfile(BIDSfirst,sublist{i},'anat') 
    matlabbatch{1}.spm.util.imcalc.input{i,1} = [subdir, filesep, sublist{i}, '_space-MNI152NLin2009cAsym_label-GM_probseg.nii,1'] 
end 
 
matlabbatch{1}.spm.util.imcalc.output = 'averageGM'; 
matlabbatch{1}.spm.util.imcalc.outdir = {'C:\Users\LabUser\Desktop\DataFolder\fcHRI\derivatives\bids_spm\second_level'}; %change this 
matlabbatch{1}.spm.util.imcalc.expression = 'mean(X)'; 
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {}); 
matlabbatch{1}.spm.util.imcalc.options.dmtx = 1; 
matlabbatch{1}.spm.util.imcalc.options.mask = 0; 
matlabbatch{1}.spm.util.imcalc.options.interp = 1; 
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
 
%% Run matlabbatch jobs
spm_jobman('run',matlabbatch);

