   
    % Run the following (ROI_extract.m) script in matlab
    % (change the code per dataset and roi and contrast - run this in MATLAB):
    % ```{, eval = FALSE}
    %========================================================================
    % ROI analysis for fmriprep data in BIDS format
    %========================================================================
    % This script is written by Ann Hogenhuis and Ruud Hortensius
    % (Utrecht University)
    % This script is based on a script by Ruud Hortensius and Michaela Kent
	% (University of Glasgow)
    % % Last updated: December 2020
    %========================================================================
      
clear all
%add marsbar to path
marsbar('on') 

%%Inputdirs
BIDS = spm_BIDS('/Volumes/BRASC-001/fc-hri/');
BIDSsecond=fullfile(BIDS.dir, 'derivatives/bids_spm/second_level'); % get the second-level directory
 
contrastid = 'HRI_listening' 
network = {'tom', 'ppn', 'object', 'language', 'domain'}

%% Outputdirs
outputdir=fullfile(BIDS.dir, 'derivatives/roi', contrastid); % root outputdir for sublist
spm_mkdir(outputdir); % create output directory
 
%% Load design matrix
spm_name = spm_load(fullfile(BIDSsecond, filesep, contrastid , 'SPM.mat'))
D = mardo(spm_name);
 
%% Load rois
for k=1:length(network)

    parcels = dir(fullfile(BIDS.dir,'derivatives/parcels', network{k}))
    parcels = struct2cell(parcels(arrayfun(@(x) ~strcmp(x.name(1),'.'),parcels)))
    parcels(2:6,:) = []

    for i=1:length(parcels)
        roi = fullfile(BIDS.dir,'derivatives/parcels', network{k}, parcels{i})
        R = maroi(roi); % Fetch data into marsbar data object
        mY = get_marsy(R, D,'mean');
        roi_data = summary_data(mY); % get summary time course(s)
        roi_name = [outputdir,filesep,network{k},filesep,parcels{i},'.csv'];
        outputdir2 = fullfile(outputdir,filesep,network{k});
        spm_mkdir(outputdir2); % create output directory
        writematrix(roi_data, roi_name);
    end
end