%========================================================================
%     SPM second-level analysis for fmriprep data in BIDS format
%     for windows 
%========================================================================
%     This script is written by Ann Hogenhuis and Ruud Hortensius
%     This script is based on a script by Ruud Hortensius and Michaela Kent
%     (University of Glasgow)
%
%     Last updated: August 2021
%========================================================================
 
 
clear all
 
%% Inputdirs
BIDS = spm_BIDS('/Volumes/BRASC-001/fc-hri/');
BIDSfirst=fullfile('first_level1'); % get the first-level directory
 
sublist = transpose(BIDS.participants.participant_id) %get subject list including the 'sub'
subex = [10, 12, 19]
sublist(subex) = []; %update the subjects
 
taskid='convers'; %specify the task to be analysed
 
contrast='con_0001'; %specify the contrast to be analysed
contrast_name='Baseline_Interaction'; %specify the name of the contrast 
 
smoothing = 1; %smoothing of first-level contrasts (1=yes, 0=no)
s_kernel = [5 5 5] %same as Rauchbauer et al
 
%% Outputdirs
outputdir=fullfile('second_level', char(contrast_name));  % root outputdir for sublist
spm_mkdir(outputdir) 
 
spm('Defaults','fMRI'); %Initialise SPM fmri
spm_jobman('initcfg');  %Initialise SPM batch mode
 
 
%% Smoothing of first-level contrasts
if smoothing == 1
    for i=1:length(sublist)
        subdir=fullfile(BIDSfirst,sublist{i}, taskid);
        matlabbatch{1}.spm.spatial.smooth.data{i,1} = [subdir, filesep, contrast, '.nii,1'];
        matlabbatch{1}.spm.spatial.smooth.fwhm = s_kernel;
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';
    end
    spm_jobman('run',matlabbatch);
    
    clear matlabbatch
end
 
 
%% Load the contrasts
matlabbatch{1}.spm.stats.factorial_design.dir = {outputdir};
 
for i=1:length(sublist)
    subdir=fullfile(BIDSfirst,sublist{i}, taskid);
    if smoothing == 1
        matlabbatch{1,1}.spm.stats.factorial_design.des.t1.scans{i,1} = [subdir, filesep, 's', contrast, '.nii,1']
    else
        matlabbatch{1,1}.spm.stats.factorial_design.des.t1.scans{i,1} = [subdir, filesep, contrast, '.nii,1']
    end
end
 
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
 
%% Model estimation
matlabbatch{2}.spm.stats.fmri_est.spmmat = {[outputdir filesep 'SPM.mat']};
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
 
%% Contrast
%--------------------------------------------------------------------------
matlabbatch{3}.spm.stats.con.spmmat = {[outputdir filesep 'SPM.mat']};
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = contrast_name;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = 1;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 0;
 
%% Results
%--------------------------------------------------------------------------
matlabbatch{4}.spm.stats.results.spmmat = {[outputdir filesep 'SPM.mat']};
matlabbatch{4}.spm.stats.results.conspec.titlestr = '';
matlabbatch{4}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{4}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{4}.spm.stats.results.conspec.thresh = 0.001;
matlabbatch{4}.spm.stats.results.conspec.extent = 10;
matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{4}.spm.stats.results.conspec.mask.image.name = {'second_level/averageGM.nii,1'}; 
matlabbatch{4}.spm.stats.results.conspec.mask.image.mtype = 0;
matlabbatch{4}.spm.stats.results.units = 1;
matlabbatch{4}.spm.stats.results.export{1}.pdf = true;
matlabbatch{4}.spm.stats.results.export{2}.jpg = true;
matlabbatch{4}.spm.stats.results.export{3}.csv = true;
matlabbatch{4}.spm.stats.results.export{4}.tspm.basename = contrast_name;
 
%% Run matlabbatch jobs
spm_jobman('run',matlabbatch);

