%>> %========================================================================
%     SPM first-level analysis for fmriprep data in BIDS format
%========================================================================
%     This script is written by Ann Hogenhuis and Ruud Hortensius
%     (Utrecht University). Based upon a script written by
%     Ruud Hortensius and Michaela Kent.
%
%
%     Last updated: May 2021
%========================================================================
 
%clear all
 
%% Inputdirs
BIDS = spm_BIDS('/Volumes/BRASC-001/fc-hri/');
BIDSpreproc=fullfile(BIDS.dir,'derivatives/fmriprep');
 
%sublist = spm_BIDS(BIDS,'01') %number of subjects
sublist = transpose(BIDS.participants.participant_id) %get subject list including the 'sub
subex = [10, 12, 19]
sublist(subex) = []; %update the subjects
 
taskid='convers'; %specify the task to be analysed
nruns = spm_BIDS(BIDS,'runs','task',taskid) %get the number of runs
nR = 4; 
numScans=385;  %The number of volumes per run <---
 
TR= 1.205;   % Repetition time, in seconds <---
unit='secs'; % onset times in secs (seconds) or scans (TRs)
 
%% Outputdirs
outputdir=fullfile(BIDS.dir, 'derivatives/bids_spm/first_level3');  % root outputdir for sublist
spm_mkdir(outputdir,char(sublist), char(taskid)); % create output directory
 
%% Loop for sublist
spm('Defaults','fMRI'); %Initialise SPM fmri
spm_jobman('initcfg');  %Initialise SPM batch mode
%r = [1 2 3 4];
%runs = size(r,3);


for i=2:length(sublist)
   
    %% Output dirs where you save SPM.mat
    subdir=fullfile(outputdir,sublist{i},taskid);
 
    %% Basic parameters
    matlabbatch{1}.spm.stats.fmri_spec.dir = {subdir};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = unit; % specified above
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = TR; % specified above
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 54;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 27; 
 
    %% Load input files for task specilized
    sub_inputdir=fullfile(BIDSpreproc,sublist{i},'func');
    sub_inputdirA=fullfile(BIDSpreproc,sublist{i},'anat');
    sub_inputdirE=fullfile(BIDS.dir,'derivatives/events-new');

    for jj=1:nR
        rr = num2str(jj)
        %------------------------------------------------------------------
        func=[sub_inputdir,filesep,sublist{i},'_task-',taskid,'_run-',rr,'_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'];
        func_nii=[sub_inputdir,filesep,sublist{i}, '_task-',taskid,'_run-',rr,'_space-MNI152NLin2009cAsym_desc-preproc_bold.nii'];
        if ~exist(func_nii,'file'), gunzip(func)
        end
        run_scans = spm_select('Expand',func_nii);

        matlabbatch{1}.spm.stats.fmri_spec.sess(jj).scans = cellstr(run_scans);
        
        % Load the condition files
        events = spm_load([sub_inputdirE,filesep, sublist{i},'_task-',taskid,'_run-0',rr,'_new-events.tsv']) %load TSV condition file

        names{1} = 'CONV1_listening'; %CONV1 = HHI and CONV2 = HRI
        t = strcmp(names{1}, events.trial_type)
        onsets{1} = transpose(events.onset(t));
        durations{1} = transpose(events.duration(t));
        
        names{2} = 'CONV2_listening';
        t = strcmp(names{2}, events.trial_type)
        onsets{2} = transpose(events.onset(t));
        durations{2} = transpose(events.duration(t));
 
        names{3} = 'CONV1_speaking';
        t = strcmp(names{3}, events.trial_type)
        onsets{3} = transpose(events.onset(t));
        durations{3} = transpose(events.duration(t));
        
        names{4} = 'CONV2_speaking';
        t = strcmp(names{4}, events.trial_type)
        onsets{4} = transpose(events.onset(t));
        durations{4} = transpose(events.duration(t));
                    
        file_mat = [subdir,filesep,sublist{i},'_task-',taskid,'_run-',rr,'_conditions.mat'];
        save(file_mat, 'names', 'onsets', 'durations')
        matlabbatch{1}.spm.stats.fmri_spec.sess(jj).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(jj).multi = {file_mat};
        
        % Confounds file
        confounds=spm_load([sub_inputdir,filesep,sublist{i},'_task-',taskid,'_run-',rr,'_desc-confounds_timeseries.tsv'])  ;
        confounds_matrix=[confounds.framewise_displacement, confounds.a_comp_cor_00,confounds.a_comp_cor_01,confounds.a_comp_cor_02,confounds.a_comp_cor_03, confounds.a_comp_cor_04,confounds.a_comp_cor_05, confounds.trans_x, confounds.trans_y, confounds.trans_z, confounds.rot_x, confounds.rot_y, confounds.rot_z];
        confounds_name=[subdir,filesep,sublist{i},'_task-',taskid,'_run-',rr,'_acomcorr.txt'];
        
        confounds_matrix(isnan(confounds_matrix)) = 0 % nanmean(confounds_matrix);
 
        if ~exist(confounds_name,'file'), dlmwrite(confounds_name,confounds_matrix)
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(jj).multi_reg = {confounds_name};
        matlabbatch{1}.spm.stats.fmri_spec.sess(jj).hpf = 128; % High-pass filter (hpf) without using consine
    end
    
    %% Model  (Default)
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'Scaling';
    mask=[sub_inputdirA,filesep,sublist{i},'_space-MNI152NLin2009cAsym_label-GM_probseg.nii.gz'];
    mask_nii=[sub_inputdirA,filesep,sublist{i},'_space-MNI152NLin2009cAsym_label-GM_probseg.nii'];
 
    if ~exist(mask_nii,'file'), gunzip(mask)
    end
    mask_nii=[mask_nii, ',1']
    matlabbatch{1}.spm.stats.fmri_spec.mask = {mask_nii};
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
    matlabbatch{1}.spm.stats.fmri_spec.cvi = 'none';
 
    %% Model estimation (Default)subdir
    matlabbatch{2}.spm.stats.fmri_est.spmmat = {[subdir filesep 'SPM.mat']};
    matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
 
    %% Contrasts
    matlabbatch{3}.spm.stats.con.spmmat = {[subdir filesep 'SPM.mat']};
    % Set contrasts of interest. %con1 - con2, listening, speaking CONV1 = HHI and CONV2 = HRI
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'HHI_listening';
    matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = repmat([1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0],1,nR);   
    matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'HHI_speaking';
    matlabbatch{3}.spm.stats.con.consess{2}.tcon.convec = repmat([0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0],1,nR);   
    matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'HRI_listening';
    matlabbatch{3}.spm.stats.con.consess{3}.tcon.convec = repmat([0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0],1,nR); 
    matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = 'HRI_speaking';
    matlabbatch{3}.spm.stats.con.consess{4}.tcon.convec = repmat([0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0],1,nR); 
    matlabbatch{3}.spm.stats.con.delete = 0;
 
    %% Run matlabbatch jobs
    spm_jobman('run',matlabbatch);
 
end

