    % This script loads MNI coordinates specified in a user-created file,
    % spherelist.txt, and generates .mat and .img ROI files for use with
    % Marsbar, MRIcron etc.  spherelist.txt should list the centres of
    % desired spheres, one-per-row, with coordinates in the format:
    % X1 Y1 Z1
    % X2 Y2 Z2 etc
    % .mat sphere ROIs will be saved in the script-created mat directory.
    % .img sphere ROIs will be saved in the script-created img directory.
    % SPM Toolbox Marsbar should be installed and started before running script.
    % Adapted from: http://akiraoconnor.org/2010/08/18/marsbar-script-save-spheres-as-both-img-and-mat-files/

    % specify radius of spheres to build in mm
    radiusmm = 9;

    %load('spherelist.txt')
    
    opts = detectImportOptions('spherelist2.txt');
    opts.VariableTypes{4} = 'string';
    spherelist2 = readtable('spherelist2.txt', opts)
    spherelist = table2array(spherelist2);

    %spherelist2 = spherelist2(4)
    % Specify Output Folders for two sets of images (.img format and .mat format)
    roi_dir_img = 'img';
    roi_dir_mat = 'mat';
    % Make an img and an mat directory to save resulting ROIs
    if ~isdir(roi_dir_img)
        mkdir(roi_dir_img);
    end
    
    if ~isdir(roi_dir_mat)
        mkdir(roi_dir_mat);
    end
    % Go through each set of coordinates from the specified file (line 2)
    spherelistrows = length(spherelist(:,1));
    for spherenumbers = 1:spherelistrows
    % maximum is specified as the centre of the sphere in mm in MNI space
    maximum = double(spherelist(spherenumbers,1:3));
    sphere_centre = maximum;
    sphere_radius = radiusmm;
    sphere_roi = maroi_sphere(struct('centre', sphere_centre, 'radius', sphere_radius));

    % Define sphere name using coordinates
    coordsx = num2str(maximum(1));
    coordsy = num2str(maximum(2));
    coordsz = num2str(maximum(3));
    
    name = spherelist(spherenumbers,4);
    spherelabel = sprintf('%s', name);
    sphere_roi = label(sphere_roi, spherelabel);
    
    % save ROI as MarsBaR ROI file
    saveroi(sphere_roi, fullfile(roi_dir_mat, sprintf('%s_roi.mat', spherelabel)));

    % Save as image
    save_as_image(sphere_roi, fullfile(roi_dir_img, sprintf('%s_roi.nii', spherelabel)));

    end